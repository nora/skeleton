<?php

declare(strict_types=1);

namespace __Vendor__\__Package__\Resource\Page;

use BEAR\Resource\ResourceObject;

class Index extends ResourceObject
{
    public function __construct()
    {
    }

    public function onGet(): ResourceObject
    {
        $this['test'] = 'ok';
        return $this;
    }
}
