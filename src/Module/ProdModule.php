<?php

namespace __Vendor__\__Package__\Module;

use BEAR\Package\Context\ProdModule as PackageProdModule;
use BEAR\QueryRepository\CacheVersionModule;
use BEAR\Resource\Module\OptionsMethodModule;
use BEAR\Package\AbstractAppModule;

class ProdModule extends AbstractAppModule
{
    protected function configure()
    {
        $this->install(new PackageProdModule);
        $this->override(new OptionsMethodModule);
        $this->install(new CacheVersionModule('1'));
    }
}
