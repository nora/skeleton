<?php

declare(strict_types=1);

namespace NORA\Skeleton;

use Closure;
use Composer\Factory;
use Composer\Json\JsonFile;
use Composer\Script\Event;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

final class Installer
{
    private static string $packageName;
    private static string $userName;
    private static string $userEmail;
    /** @var array<string> */
    private static array $appName;

    public static function install(Event $event): void
    {
        $composer_file = Factory::getComposerFile();
        $dirname = dirname($composer_file);
        $nora_file = $dirname . "/nora.json";
        if (!is_file($nora_file)) {
            throw new \RuntimeException("$nora_file is not readable");
        }
        $nora_json = new JsonFile($nora_file);
        $nora_data = $nora_json->read();
        if (!is_array($nora_data)) {
            throw new \RuntimeException("$nora_file is broken");
        }
        assert(is_string($nora_data["vendor"]));
        assert(is_string($nora_data["package"]));
        assert(is_string($nora_data["author"]["name"]));
        assert(is_string($nora_data["author"]["email"]));
        self::$packageName = vsprintf(
            "%s/%s",
            array_map(
                fn ($name) => self::camel2dashed($name),
                [$nora_data['vendor'], $nora_data['package']]
            )
        );
        self::$userName = $nora_data['author']['name'];
        self::$userEmail = $nora_data['author']['email'];

        $json = new JsonFile($composer_file);
        $composerDefinition = self::getDefinition(
            $nora_data['vendor'],
            $nora_data['package'],
            self::$packageName,
            $json
        );

        $json->write($composerDefinition);
        self::$appName = [
            $nora_data['vendor'],
            $nora_data['package'],
        ];

        [$vendorName, $packageName] = self::$appName;
        $skeletonRoot = dirname(__DIR__);
        // Replace __Vendor__,__Package__,etc
        self::recursiveJob($skeletonRoot, self::rename($vendorName, $packageName));
        copy($skeletonRoot . '/src/Skeleton.php', "{$skeletonRoot}/src/{$packageName}.php");
        copy($skeletonRoot . '/tests/SkeletonTest.php', "{$skeletonRoot}/tests/{$packageName}Test.php");

        unlink($skeletonRoot . '/README.md');
        unlink($skeletonRoot . '/src/Skeleton.php');
        unlink($skeletonRoot . '/tests/SkeletonTest.php');
        unlink(__FILE__);

        passthru(dirname(__DIR__) . '/vendor/bin/composer install');
        shell_exec(dirname(__DIR__) . '/vendor/bin/phpcbf');
        shell_exec(dirname(__DIR__) . '/vendor/bin/composer dump-autoload --quiet');
        shell_exec(dirname(__DIR__) . '/vendor/bin/psalm --init > /dev/null');

        rename($skeletonRoot . '/README.proj.md', $skeletonRoot . '/README.md');
        rename($skeletonRoot . '/.gitattributes.txt', $skeletonRoot . '/.gitattributes');
        $io = $event->getIO();
        $io->write(sprintf('<info>%s package created.</info>', self::$packageName));
        $io->write('<info>Happy quality coding!</info>');
        $io->write('<info>Thanks to Koriym.</info>');
    }

    private static function recursiveJob(string $path, callable $job): void
    {
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
        foreach ($iterator as $file) {
            $job($file);
        }
    }

    private static function rename(string $vendor, string $package): Closure
    {
        return static function (SplFileInfo $file) use ($vendor, $package): void {
            $fileName = $file->getFilename();
            $filePath = (string) $file;
            if ($file->isDir() || strpos($fileName, '.') === 0 || !is_writable($filePath)) {
                return;
            }

            $contents = (string) file_get_contents($filePath);
            $search = ['__Vendor__', '__Package__', '__year__', '__name__', '__PackageVarName__', '_package_name_'];
            $replace = [$vendor, $package, date('Y'), self::$userName, lcfirst($package), self::$packageName];
            $contents = str_replace($search, $replace, $contents);
            file_put_contents($filePath, $contents);
        };
    }

    private static function camel2dashed(string $name): string
    {
        return strtolower((string) preg_replace('/([a-zA-A])(?=[A-Z])/', '$1-', $name));
    }

    /**
     * @return (mixed|string|string[][])[]
     *
     * @psalm-return array{name: string, authors: list{array{name: string, email: string}}, description: '', autoload: mixed, scripts: mixed,...}
     */
    private static function getDefinition(string $vendor, string $package, string $packageName, JsonFile $json): array
    {
        $composerDefinition = $json->read();
        if (!is_array($composerDefinition)) {
            throw new \RuntimeException("Broken composer.json detected");
        }
        unset(
            $composerDefinition['autoload']['files'],
            $composerDefinition['scripts']['pre-install-cmd'],
            $composerDefinition['scripts']['post-install-cmd'],
            $composerDefinition['scripts']['pre-update-cmd'],
            $composerDefinition['scripts']['post-create-project-cmd'],
            $composerDefinition['keywords'],
            $composerDefinition['homepage'],
            $composerDefinition['require-dev']['composer/composer'],
        );
        $composerDefinition['name'] = $packageName;
        $composerDefinition['authors'] = [
            [
                'name' => self::$userName,
                'email' => self::$userEmail,
            ],
        ];
        $composerDefinition['description'] = '';
        $composerDefinition['autoload']['psr-4'] = ["{$vendor}\\{$package}\\" => 'src/'];
        $composerDefinition['scripts']['post-install-cmd'] = '@composer bin all install --ansi';
        $composerDefinition['scripts']['post-update-cmd'] = '@composer bin all update --ansi';

        return $composerDefinition;
    }
}
