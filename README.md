# Nora Skelton

## 使い方

### create files

```
composer create-project nora/skelton path/to/directory
cd path/to/directory
```

### create nora.json

```
{
    vendor: "NORA",
    package: "Skeleton",
    author: {
        name: "Hajime",
        email: "hajime.matsumoto@avap.co.jp"
    }
}
```

### apply settings

```
composer nora-install
```

### create .env

```
DOCKER_UID=15000
DOCKER_GID=15000
```

