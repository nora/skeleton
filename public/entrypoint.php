<?php

declare(strict_types=1);

use __Vendor__\__Package__\Bootstrap;

// 日本時間をセットする
date_default_timezone_set('Asia/Tokyo');

require realpath(__DIR__ . '/..') . '/vendor/autoload.php';
exit((new Bootstrap())(PHP_SAPI === 'cli' ? 'cli-hal-app' : 'hal-app', $GLOBALS, $_SERVER));

